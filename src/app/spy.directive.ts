import { Directive, OnDestroy, OnInit } from "@angular/core";

let nextId = 1;

@Directive({selector: '[appSpy]'})
export class SpyDirective implements OnInit, OnDestroy {
  private id = nextId++;

  ngOnInit() {
    console.log(`Spy #${this.id} OnInit`);
  }

  ngOnDestroy() {
    console.log(`Spy #${this.id} OnDestroy`);
  }
}
